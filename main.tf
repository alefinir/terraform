#set provider and information account
provider "aws" {
  access_key = "XXXX"
  secret_key = "XXXXXXXXXX"
  region     = "us-east-2"
}

#create security group for ec2 instance
#with full access to your ip and open ports 80 and 443 for public access
resource "aws_security_group" "terra_sg" {
  name        = "terra_sg"
  description = "Allow all from your ip"
#your ips
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["x.x.x.x/32"]
  }
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["x.x.x.x/32"]
  }
#ports 80 and 443 for everyone
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
  tags = {
    Name = "terra_sg"
  }
}

#create a t2.micro instance ubuntu16.04 and key terra.pem
resource "aws_instance" "example" {
  ami           = "ami-0653e888ec96eab9b"
  instance_type = "t2.micro"
  key_name = "terra"
  security_groups = [ "terra_sg" ]
  tags {
    Name = "terraform.test.com"
  }
}

#assign an elastic ip and show the ip
  resource "aws_eip" "ip" {
  instance = "${aws_instance.example.id}"
}
  output "ip" {
  value = "${aws_instance.example.public_ip}"
}