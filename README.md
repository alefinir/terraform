# A Demo of Terraform for AWS EC2 instance with Linux Ubuntu 16.04

This repo contains the code to build a t2.micro instance of Ubuntu16.04 and 1 security group with 2 kinds of rules:

* The first two rules open all ports for your ips
* The second rule opens ports 80 and 443 for all

The key (terra.pem) used for acces the instance was previously created in Network & Security / Key Pairs menu.

Finally, it is assigned an elastic ip to the instance recently created.

## You have to change the credential access to aws, update the ips and create a keypairs.

To build the infraestructure from the main.tf file, run the following command from inside this directory:

```sh
$ terraform init
```
This will produce the following output:

```sh
Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

To test your main.tf, use the following command:
```sh
$ terraform plan
```

To deploy the server on aws, use the following command:
```sh
$ terraform apply
```

To acces to new ec2 instance use the following command: 

```sh
$ ssh -i terra.pem ubuntu@new_elastic_ip_assigned
```